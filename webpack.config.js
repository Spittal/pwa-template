const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

module.exports = env => {
  let config = {
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].[hash].js',
      sourceMapFilename: '[name].[hash].map'
    },
    devtool: 'cheap-module-source-map',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              'css-loader',
              'postcss-loader'
            ]
          })
        }
      ]
    },
    devServer: {
      contentBase: './dist',
      hot: true
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html'
      }),
      new webpack.HotModuleReplacementPlugin(),
      new ExtractTextPlugin('[name].[hash].css'),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'common'
      })

    ]
  };

  if (env === 'production') {
    config.plugins.push(
      new SWPrecacheWebpackPlugin({
        staticFileGlobsIgnorePatterns: [/\.map$/],
        navigateFallback: 'index.html',
        runtimeCaching: [
          {
            urlPattern: /.*api.*/,
            handler: 'networkOnly',
            method: 'any'
          }
        ]
      })
    );
  }

  return config;
};
